<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = "answers";
    protected $fillable = ["description", "answer_id", "user_id", "question_id"];
    protected $dates = ["created_at", "update_at"];

    public function user()
    {
        return $this->belongsTo("App\User");
    }

    public function question()
    {
        return $this->belongsTo("App\Question");
    }
}
