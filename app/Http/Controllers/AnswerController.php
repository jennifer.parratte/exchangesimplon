<?php namespace App\Http\Controllers;

use App\Http\Requests\StoreAnswer;
use App\Repositories\AnswerRepository;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    protected $answerRepository;

    public function __construct(AnswerRepository $answerRepository)
    {
        $this->middleware('auth');
        $this->answerRepository = $answerRepository;
    }

    public function store(Request $request)
    {
        $requestData = $request->all();
        $requestData ['user_id'] = auth()->user()->id;
        $this->answerRepository->create($requestData);

        return back();
    }
}