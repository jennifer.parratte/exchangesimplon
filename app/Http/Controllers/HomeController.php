<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\QuestionRepository;
use App\Repositories\AnswerRepository;

class HomeController extends Controller
{
    protected $questionRepository;
    

    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(AnswerRepository $answerRepository)
    {
        $questions = $this->questionRepository->getOrderedQuestions();
        $recentQuestions = $this->questionRepository->getRecentQuestions(2);
        $answersCount = $answerRepository->getAnswers();
        return view('homepage', compact('questions', 'recentQuestions', 'answersCount'));
    }
}
