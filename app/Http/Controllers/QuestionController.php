<?php namespace App\Http\Controllers;

use App\Repositories\QuestionRepository;
use App\Repositories\AnswerRepository;
use App\Http\Requests\StoreQuestion;



class QuestionController extends Controller
{
    protected $questionRepository;

    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
        $this->middleware('auth')->except('index');
    }

    public function index()
    {
        $answersCount = $this->answerRepository->getAnswers();
        $questions = $this->questionRepository->getOrderedQuestions();
        $recentQuestions = $this->questionRepository->getRecentQuestions(2);
        return view ('questions.index', compact('questions','recentQuestions', 'answersCount'));
    }

    public function create()
    {
        return view('questions.create');
    }

    public function show($id, AnswerRepository $answerRepository)
    {
        $questionId = Question::find($id);
        $questions = $this->questionRepository->getOrderedQuestions();
        $question = $this->questionRepository->show($id);
        $answers = $answerRepository->getOrderedAnswers($questionId);
        $answersCount = $this->answerRepository->getAnswers();


        return view('questions.show', compact('question', 'answers', 'questions', 'questionId', 'answersCount'));
    }

    public function store(StoreQuestion $request)
    {
        $requestData = $request->all();
        $question = $this->questionRepository->create($requestData);

        return redirect()->route('question.show', ['id' => $question->id])->with('flash_message', 'Question added');
    }

}