@extends('layouts.fullwidth')



@section('title', 'Poser une question')



@section('content')



    <div class="page-content ask-question">

        <div class="boxedtitle page-title"><h2>Ask Question</h2></div>

        <p>Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque.</p>

        <div class="form-style form-style-3" id="question-submit">
            
                {!! Form::open (['method'=>'post', 'action' => 'QuestionController@store']) !!}
                        @csrf
                    {!! Form::token(); !!}
                    <div class="form-inputs clearfix">
                <p>
                    {!! Form::label('title', 'Question title *'); !!}

                    {!! Form::text('title', 'titre', ['id' => 'question-title']); !!}

                    <span class="form-description">Please choose an appropriate title for the question to answer it even easier .</span>

                </p>

                <p>
                    {!! Form::label('text', 'Tags *'); !!}

                    {!! Form::text('text', null, ['class' => 'input', 'id' => 'question_tags', 'data-seperator' => ','] ); !!}

                    <span class="form-description">Please choose suitable Keywords .</span>
                </p>

                <p>
                    {!! Form::label('text', 'Category *', ['class' => 'required', 'id' => 'question_tags']); !!}
            
                    <span class="styled-select" id='question-title'></span>

                        {!! Form::select('category',[

                                'Back-end' => [
                                'php' => 'PHP',
                                'mysql' => 'MySQL',
                                'nodejs' => 'NodeJS',
                                'c#' => 'C#',
                                'python' => 'Python',
                                'ruby' => 'Ruby',
                                'other' => 'Other'
                                ],

                                'Front-end' => [
                                'html' => 'HTML',
                                'css' => 'CSS',
                                'sass' => 'SASS',
                                'js' => 'JS',
                                'other' => 'Other'
                                ],

                                'miscellaneous' => [
                                'agile' => 'Agile',
                                'sysadmin' => 'Admin',
                                'devops' => 'Dev Ops',
                                'other' => 'Other'
                                ],

                        ], null, ['placeholder' => 'Pick a category...', 'class' => 'styled-select']) !!}

                    @if ($errors->has('category'))
                        <span class='color form-description'>
                            <strong>{{$errors->first('category')}} </strong>
                        </span>
                    @else
                        <span class="form-description">Please choose the appropriate section so easily search for your question .</span>
                    @endif
                </p>
            </div>

            <div id="form-textarea">

                    <p>

                        {!! Form::label('description', 'Detail *', ['id'=>'question_tags', 'class' => 'required']); !!}

                        {!! Form::textarea('description', null, ['id' => 'question-title', 'aria-required' => 'true', 'cols' => 58, 'rows' => 8]) !!}

                       <!--<span class="form-description">Type the description thoroughly and in detail .</span>-->

                    

                        @if ($errors->has('description'))
                        <span class='color form-description'>
                            <strong>{{$errors->first('description')}} </strong>
                        </span>
                        @else
                        <span class="form-description">Please choose the appropriate section so easily search for your question .</span>
                        @endif
                    </p>
            </div>

                    <p class="form-submit ">

                        {!! Form::submit('Publish Your Question', ['class' => 'button color small submit', 'id' => 'publish-question']); !!}

                    </p>

                {!! Form::close() !!}
               

                        <!--<label>Tags</label>

                        <input type="text" class="input" name="question_tags" id="question_tags" data-seperator=",">

                        <label class="required">Category</label>

                            <select>

                                <option value="">Select a Category</option>

                                <option value="1">Category 1</option>

                                <option value="2">Category 2</option>

                            </select>

          
                        <label class="required">Details<span>*</span></label>

                        <textarea id="question-details" aria-required="true" cols="58" rows="8"></textarea>



                   <input type="submit" id="publish-question" value="Publish Your Question" class="button color small submit">

                

            </form>-->

        </div>

    </div><!-- End page-content -->



@endsection