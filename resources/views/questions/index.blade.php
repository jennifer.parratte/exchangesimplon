@extends('layouts.2-columns')



@section('title', 'Poser une question')



{{-- Search form section --}}

@section('search-form')

    @include('partials/search')

@endsection



@section('content')

@forelse($questions as $question)

    <article class="question question-type-normal">

        <h2>

<a href="question/{{$question->id}}">{{ $question->title }}</a>

        </h2>

        <div class="question-author-date">

            Asked <em>{{ $question->created_at->diffForHumans() }}</em> by <a href="#">{{ $question->user->name }}</a>

        </div>

        <div class="question-inner">

            <div class="clearfix"></div>

                <p class="question-desc">{{ $question->description }}</p>

            {{--<div class="question-details">--}}

                {{--<span class="question-answered question-answered-done"><i class="icon-ok"></i>solved</span>--}}

            {{--</div>--}}

            {{--<span class="question-comment"><a href="{{ url('/question') }}"><i class="icon-comment"></i>5 Answer</a></span>--}}

            {{--<span class="question-view"><i class="icon-user"></i>70 views</span>--}}

            {{--<div class="question-tags"><i class="icon-tags"></i>--}}

                {{--<a href="#!">wordpress</a>, <a href="#!">question</a>, <a href="#!">developer</a>--}}

            {{--</div>--}}

            <div class="clearfix"></div>

        </div>

    </article>


@empty
<p>No questions</p>
@endforelse

    <a href="#" class="load-questions"><i class="icon-refresh"></i>Load More Questions</a>



    {{-- Sidebar --}}

    @section('sidebar')

        <div class="widget">

            <h3 class="widget_title">Recent Questions</h3>

            <ul class="related-posts">

                @forelse($recentQuestions as $recentQuestion)
                <li class="related-item">

            <h3><a href="#">{{ $recentQuestion->title }}</a></h3>

                <p>{{ $recentQuestion->description }}</p>

        <div class="clear"></div><span>{{ $recentQuestion->created_at->diffForHumans() }}</span>

                </li>
            @empty
                <p>pas là</p>
            @endforelse
            </ul>

        </div>



        @parent

    @endsection



@endsection